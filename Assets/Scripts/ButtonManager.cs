using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    //restart the scene
    public void OnRestart()
    {
        //reload active scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //If the button exit clicked
    public void OnExit()
    {
        //exit the game
        Application.Quit();
    }

    public void OnNextLevel()
    {
        //Get the value level
        string Level = PlayerPrefs.GetString("Level");

        //Switch case the value level to run the next level
        switch (Level)
        {
            case "Level01":
                PlayerPrefs.SetString("Level", "Level02");
                break;
            case "Level02":
                PlayerPrefs.SetString("Level", "Level03");
                break;
            case "Level03":
                PlayerPrefs.SetString("Level", "Level04");
                break;
        }

        //reload active scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
