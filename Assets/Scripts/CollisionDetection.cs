using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;
    [SerializeField] private PlayerMovement player;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if the player collided on the obtacle
        if (collision.collider.CompareTag("Player") && gameObject.CompareTag("Obstacle"))
        {
            //call the function in UIManager to display gameover UI;
            uiManager.GameOver();
            //disable movement script
            player.onGameOver = true;
        }
        //if the player collided with this game object
        else if (collision.collider.CompareTag("Player") && gameObject.CompareTag("FinishLine"))
        {
            //Call the UI Level Complete
            uiManager.FinishLine();
            //disable movement script
            player.onGameOver = true;
        }
    }
}
