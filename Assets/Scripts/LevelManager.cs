using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameObject[] level;

    // Start is called before the first frame update
    void Start()
    {
        //get the value level
        string Level = PlayerPrefs.GetString("Level");

        //switch case the value then run theat level
        switch (Level){
            case "Level01":
                level[0].SetActive(true);
                break;
            case "Level02":
                level[1].SetActive(true);
                break;
            case "Level03":
                level[2].SetActive(true);
                break;
            case "Level04":
                level[3].SetActive(true);
                break;
            default:
                level[0].SetActive(true);
                break;
        }
    }
}
