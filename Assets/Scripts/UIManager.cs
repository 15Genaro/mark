using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject levelComplete;

    public void GameOver()
    {
        gameOver.SetActive(true);
    }

    public void FinishLine()
    {
        levelComplete.SetActive(true);
    }
}
