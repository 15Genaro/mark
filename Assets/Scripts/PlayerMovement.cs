using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private PlayerController2D controller;
	[SerializeField] private Animator anim;
    [SerializeField] private float speed = 400;
    private bool jump = false;

    public bool onGameOver = false;
	private void Start()
	{
		anim.SetFloat("speed",0.2f);	
	}

    private void Update()
    {
        if (onGameOver != true)
        {
            //check if the player press the space bar
            if (Input.GetKeyDown(KeyCode.Space))
			{
				//the boolean jump will be true
                jump = true;
				anim.SetBool("jump",true);
				
			}
                
        }
    }

    private void FixedUpdate()
    {

        if (onGameOver != true)
        {
            //move the player with the speed and jump
            controller.Move(speed * Time.fixedDeltaTime, false, jump);
            //disable jump so the player no longer jump.
            jump = false;
			if (jump!= true)
			anim.SetBool("jump",false);	
				
        }
        
    }
}
